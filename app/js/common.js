$(document).ready(function () {
	var task_scroll_to_description = true,
		task_scroll_to_services = true,
		task_scroll_to_portfolio = true,
		task_scroll_to_footer = true;

	device_width = $(document).width();

	/* SECTION_HEADER */

	$('#js-header-text').css('opacity', '1');

	/* TASKS_FOR_SCROLL */

	$(window).scroll(function () {
		let window_height     = $(window).height(),
			window_scroll_top = $(this).scrollTop();

		/* SECTION_DESCCRIPTION */

		let description_offset = $('#description').offset().top,
			scroll_to_section_description = (description_offset - window_height) + 250;

		if (window_scroll_top > scroll_to_section_description &&
			task_scroll_to_description) {
			$('#js-description-section-bottom').css('right', '0');

			task_scroll_to_description = false;
		}

		/* SECTION_SERVICES */

		let services_offset = $('#services').offset().top,
			scroll_to_section_services = (services_offset - window_height) + 250,
			scroll_to_section_services_technologies = scroll_to_section_services,
			services_list_items = $('#js-services-section-center').children();

			scroll_to_section_services_technologies += (device_width > 750) ? 1100 : 1800;

		if (window_scroll_top > scroll_to_section_services &&
			task_scroll_to_services) {

			$.each(services_list_items, function (i, t) {
				var $this = $(t);

				setTimeout(function () {
					$this.delay( (device_width <= 750 ) ?  1500 : 300 ).css('right', '0');
				}, i * 500);
			});
		}

		if (window_scroll_top > scroll_to_section_services_technologies) {
			if (task_scroll_to_services) {
				$.each($('#js-services-technologies').children(), function (i, t) {
					var $this = $(t);

					setTimeout(function () {
							$this.delay(300).css('right', '0');
						}, i * (device_width <= 750 ) ?  1500 : 500);
					});

				task_scroll_to_services = false;
			}
		}

		/* SECTION_PORTFOLIO */

		let portfolio_offset = $('#portfolio').offset().top,
			scroll_to_section_portfolio = (portfolio_offset - window_height);

			scroll_to_section_portfolio += (device_width > 750) ? 300 : -350;

		if (window_scroll_top > scroll_to_section_portfolio &&
			task_scroll_to_portfolio) {
			$('#portfolio').css({'transform' : 'scale(1)', 'opacity' : '1'});

			setTimeout(function() {
				$.each($('#js-portfolio-work').children(), function (i, t) {
					let $this = $(t);

					setTimeout(function () {
						$this.delay(300).css('opacity', '1');
					}, i * 500);
				});

				task_scroll_to_portfolio = false;
			}, 1000);

			console.log(scroll_to_section_portfolio);
		}

		/* SECTION_FOOTER */

		let footer_offset = $('#footer').offset().top,
			scroll_to_section_footer = (footer_offset - window_height) + 500,
			width_line = 0;

		if (window_scroll_top > scroll_to_section_footer &&
			task_scroll_to_footer) {
			$('#js-footer-section-left').css({'right' : '0', 'transform' : 'rotate(0deg) scale(1)', 'opacity' : '1'});

			setTimeout(function () {
				$('#js-footer-section-right').css('opacity', '1');
			}, 1500);

			task_scroll_to_footer = false;
		}
	});

	/* HEADER_MOBILE_MENU */

	$(document).on('click', '#js-header-menu-btn', function () {
		$('nav').toggleClass('header-menu__active')
	});

	/* POPUP */

	setTimeout(function () {
		let html = '';

		html += '<section class="popup" id="popup">' +
					'<div class="popup__content" id="js-popup-content">' +
						'<button class="popup__content-btn-close" id="js-popup-btn-close">' +
							'<figure class="popup__content-btn-line"></figure>' +
							'<figure class="popup__content-btn-line"></figure>' +
						'</button>' +
					'</div>' +
				'</section>';

			
		$('body').append(html);

		setTimeout(function () {
			$('#popup').css('opacity' , '1');

			$('body').css('overflow', 'hidden');

			setTimeout(function () {
				$("#js-popup-content").css('top', '0');
			}, 1500);
		}, 2000);

		$(document).on('click', '#js-popup-btn-close', function () {
			$('#js-popup-content').css('right', '-100%');
			$('#popup').delay(600).css('opacity' , '0');

			setTimeout(function () {
				$('#popup').remove();

				$('body').css('overflow', 'overlay');
			}, 500);
		});
	}, 5000);

	/* SECTION_DESCRIPTION */

	var device_width = 0;

	changeDescriptionImage();

	$(window).resize(changeDescriptionImage);

	function changeDescriptionImage() {
		let src_image = '';

		device_width = $(document).width();

		if (device_width <= 800) {
			src_image = 'img/description/description-image-mobile.png';
		} else {
			src_image = 'img/description/description-image-desctop.png';
		}

		$('#js-description-image').attr('src', src_image);
	}

	/* SECTION_FOOTER_FORM_VALIDATER */

	$(document).on('click', '#js-form-btn-submit', function (event) {
		var reg_name_and_sapient = /^[a-zA-Za-яA-я]+$|^30$/,
			reg_email            = /^[a-zA-Z0-9_\.-]+@[a-zA-Z_]+?\.[a-zA-Z]{2,6}$|^65$/,
			reg_number           = /^\+[0-9]+$|^20$/,
			input_name           = $('input[name="name"]'),
			input_sapient        = $('input[name="sapient"]'),
			input_email          = $('input[name="email"]'),
			input_number         = $('input[name="number"]'),
			textarea             = $('textarea[name="enquiry"]');

		var test_name = reg_name_and_sapient.test($('input[name="name"]').val()),
			test_sapient = reg_name_and_sapient.test(input_sapient.val()),
			test_email = reg_email.test(input_email.val()),
			test_number = reg_number.test(input_number.val());

		event.preventDefault();

		(!reg_name_and_sapient.test(input_name.val())) ? input_name.addClass('error') : input_name.removeClass('error');
		(!reg_name_and_sapient.test(input_sapient.val())) ? input_sapient.addClass('error') : input_sapient.removeClass('error');

		if (reg_number.test(input_number.val()) && $.trim(input_number.val()).length > 9) {
			input_number.removeClass('error');
		} else {
			input_number.addClass('error');
		}

		($.trim(textarea.val()).length <= 0) ? textarea.addClass('error') : textarea.removeClass('error');
		(!reg_email.test(input_email.val())) ? input_email.addClass('error') : input_email.removeClass('error'); 


		if (reg_name_and_sapient && reg_email && reg_number && $.trim(textarea.val()).length > 0) {
			$('form').submit();
		} else {
			return false;
		}
	})
});